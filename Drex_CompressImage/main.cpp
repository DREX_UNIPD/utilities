#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main( int argc, char** argv ) {
	// Verifico i parametri
	if (argc < 3) {
		cout << "Usage: Drex_CompressImage <source_image_path> <destination_image_path> <width = 640> <height = 480>" << endl;
		return 0;
	}
	
	int w, h;
	
	if (argc >= 5) {
		w = atoi(argv[3]);
		h = atoi(argv[4]);
	}
	else {
		w = 640;
		h = 480;
	}
	
	string source = argv[1];
	string destination = argv[2];
	
	Mat s_img, d_img;
	s_img = imread(source);
	
	Size d_size(w, h);
	resize(s_img, d_img, d_size);
	
	imshow("Original", s_img);
	imshow("Compressed", d_img);
	
	waitKey(0);
	
	imwrite(destination, d_img);
	
	return 0;
}
