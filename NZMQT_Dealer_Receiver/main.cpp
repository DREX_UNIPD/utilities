#include <iostream>

#include <nzmqtdealerreceiver.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 2) {
		cout << "Usage: ./NZMQT_Dealer_Receiver <[>@]tcp://address:port>" << endl;
		return 0;
	}
	
	// Creo un oggetto DEALER e avvio la coda dei messaggi
	NZMQTDealerReceiver a(argc, argv);
	
	return a.exec();
}
