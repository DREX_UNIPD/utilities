QT += core
QT -= gui

CONFIG += c++11

TARGET = NZMQT_Dealer_Receiver
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
	nzmqtdealerreceiver.cpp

INCLUDEPATH += \
	../ \
	../nzmqt/include

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

HEADERS += \
	nzmqtdealerreceiver.h \
	../nzmqt/include/nzmqt/nzmqt.hpp

