#include <czmq.h>
#include <iostream>

#include <string>
#include <strings.h>
#include <stdint.h>

using namespace std;

int main(int argc, char **argv) {
	// Verifico se sono presenti i parametri richiesti
	if (argc < 2) {
		cout << "Usage: CZMQ_Pub <endpoint> <frames...>" << endl;
		return 0;
	}
	
	// Creo un nuovo socket
	zsock_t *pub = zsock_new_pub(argv[1]);
	assert(pub);
	
	// Imposto il tempo di attesa prima di terminare l'invio dei messaggi nel buffer
	zsock_set_linger(pub, -1);
	
	// Creo il messaggio e lo invio
	zmsg_t *msg;
	zframe_t *frame;
	
	msg = zmsg_new();
	
	for (int i = 2; i < argc; i++) {
		frame = zframe_new(argv[i], strlen(argv[i]));
		zmsg_append(msg, &frame);
	}
	
	zmsg_print(msg);
	zmsg_send(&msg, pub);
	
	// Distruggo il socket
	zsock_destroy(&pub);
	
	return 0;
}
