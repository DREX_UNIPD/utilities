//
//  Report 0MQ version
//
#include "czmq.h"
#include <iostream>

using namespace std;

int main() {
	int v_major, v_minor, v_patch;
	
	zsys_version(&v_major, &v_minor, &v_patch);
	cout << "CZMQ Version: " << v_major << "." << v_minor << "." << v_patch << endl;
	
	zmq_version(&v_major, &v_minor, &v_patch);
	cout << "ZMQ Version: " << v_major << "." << v_minor << "." << v_patch << endl;
	
	return EXIT_SUCCESS;
}
