#include <nzmqtlogsub.h>

#include <QDateTime>
#include <iostream>

using namespace std;
using namespace nzmqt;

NZMQTLogSub::NZMQTLogSub(int argc, char **argv) : QCoreApplication(argc, argv) {
	// Creo gli oggetti
	_context = createDefaultContext();
	_context->start();
	_socket = _context->createSocket(ZMQSocket::TYP_SUB, this);
	
	// Creo i filtri
	_socket->setOption(ZMQSocket::OPT_SUBSCRIBE, "LOG");
	
	// Connetto il socket
	string connection_string = argv[1];
	string endpoint;
	switch (connection_string[0]) {
	case '@':
		endpoint = connection_string.substr(1);
		_socket->bindTo(endpoint.c_str());
		break;
	case '>':
		endpoint = connection_string.substr(1);
		_socket->connectTo(endpoint.c_str());
		break;
	default:
		_socket->connectTo(connection_string.c_str());
		break;
	}
	
	// Collego il socket allo slot
	QObject::connect(_socket, &ZMQSocket::messageReceived, this, &NZMQTLogSub::recvMessage, Qt::QueuedConnection);
}

QString NZMQTLogSub::formatLogMessage(const Message &msg) {
	if (msg.length() < 2) return "";
	
	QString output = "";
	QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
	
	// Stampo il messaggio ricevuto
	output += time;
	output += " ";
	output += QString(msg[1]);
	return output;
}

void NZMQTLogSub::recvMessage(const Message &msg) {
	QString msg_formatted;
	msg_formatted = formatLogMessage(msg);
	cout << msg_formatted.toStdString() << endl;
}
