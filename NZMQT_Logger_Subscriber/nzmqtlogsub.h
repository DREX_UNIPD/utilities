#ifndef NZMQTLOGSUB_H
#define NZMQTLOGSUB_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>

class NZMQTLogSub : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
	// Formattazione del messaggio
	QString formatLogMessage(const Message &msg);
	
public:
	// Costruttore
	NZMQTLogSub(int argc, char **argv);
	
private slots:
	void recvMessage(const Message &msg);
};

#endif // NZMQTLOGSUB_H
