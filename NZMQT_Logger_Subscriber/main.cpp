#include <iostream>

#include <nzmqtlogsub.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 2) {
		cout << "Usage: ./NZMQT_Logger_Subscriber <[>@]tcp://address:port>" << endl;
		return 0;
	}
	
	// Creo un oggetto SUB e avvio la coda dei messaggi
	NZMQTLogSub a(argc, argv);
	
	return a.exec();
}
