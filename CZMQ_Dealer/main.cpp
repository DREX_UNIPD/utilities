#include <czmq.h>
#include <iostream>

#include <string>
#include <strings.h>
#include <stdint.h>

using namespace std;

int main(int argc, char **argv) {
	// Verifico se sono presenti i parametri richiesti
	if (argc < 2) {
		cout << "Usage: dealer <endpoint> [<frame>, ...]" << endl;
		return 0;
	}
	
	// Creo un nuovo socket
	zsock_t *dealer = zsock_new_dealer(argv[1]);
	assert(dealer);
	
	// Imposto il tempo di attesa prima di terminare l'invio dei messaggi nel buffer
	zsock_set_linger(dealer, -1);
	
	// Creo un messaggio e lo compongo
	zmsg_t *msg = zmsg_new();
	for (int i = 2; i < argc; i++) {
		zframe_t *frame = zframe_new(argv[i], strlen(argv[i]));
		zmsg_append(msg, &frame);
	}
	cout << "Message sent\n" << endl;
	zmsg_print(msg);
	
	// Invio il messaggio
	zmsg_send(&msg, dealer);
	
	// Ricevo un messaggio, lo visualizzo e lo distruggo
	msg = zmsg_recv(dealer);
	cout << "Message received\n" << endl;
	zmsg_print(msg);
	zmsg_destroy(&msg);
	
	// Distruggo il socket
	zsock_destroy(&dealer);
	
	return 0;
}
