#include <nzmqtsubflight.h>

#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDateTime>
#include <iostream>

using namespace std;
using namespace nzmqt;

NZMQTSubFlight::NZMQTSubFlight(int argc, char **argv) : QCoreApplication(argc, argv) {
	// Creo gli oggetti
	_context = createDefaultContext();
	_context->start();
	_socket = _context->createSocket(ZMQSocket::TYP_SUB, this);
	
	// Creo i filtri
	for (int i = 2; i < argc; i++) {
		_socket->setOption(ZMQSocket::OPT_SUBSCRIBE, argv[i]);
	}
	
	// Connetto il socket
	string connection_string = argv[1];
	string endpoint;
	switch (connection_string[0]) {
	case '@':
		endpoint = connection_string.substr(1);
		_socket->bindTo(endpoint.c_str());
		break;
	case '>':
		endpoint = connection_string.substr(1);
		_socket->connectTo(endpoint.c_str());
		break;
	default:
		_socket->connectTo(connection_string.c_str());
		break;
	}
	
	// Collego il socket allo slot
	QObject::connect(_socket, &ZMQSocket::messageReceived, this, &NZMQTSubFlight::recvMessage, Qt::QueuedConnection);
}

QString NZMQTSubFlight::formatMessage(const Message &msg) {
	QString output = "";
	QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
	
	// Stampo il messaggio ricevuto
	output += time;
	output += "\n";
	
	int max = time.length();
	for (int i = 0; i < msg.length(); i++) {
		QString frame(msg[i]);
		
		if (i == 0) {
			QRegularExpression regex("J(?:B|M)-(?:MP|SAP|SVP)-(?:LOG|DATA)-(?<frame>.*)");
			QRegularExpressionMatch match;
			match = regex.match(frame);
			frame = match.captured("frame");
		}
		
		if (max < frame.length() + 6) max = frame.length() + 6;
		output += "[";
		output += QString("%1").arg(frame.length(), 3, 10, QChar('0'));
		output += "] ";
		output += frame;
		output += "\n";
	}
	
	output += QString("-").repeated(max);
	output += "\n";
	
	return output;
}

void NZMQTSubFlight::recvMessage(const Message &msg) {
	QString msg_formatted;
	msg_formatted = formatMessage(msg);
	cout << msg_formatted.toStdString();
}
