#ifndef NZMQTSUBSPEC_H
#define NZMQTSUBSPEC_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>

class NZMQTSubFlight : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
	// Formattazione del messaggio
	QString formatMessage(const Message &msg);
	
public:
	// Costruttore
	NZMQTSubFlight(int argc, char **argv);
	
private slots:
	void recvMessage(const Message &msg);
};

#endif // NZMQTSUBSPEC_H
