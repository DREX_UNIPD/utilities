#include <iostream>

#include <nzmqtsubflight.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 3) {
		cout << "Usage: ./NZMQT_Subscriber_Flight <[>@]tcp://address:port> <filter ...>" << endl;
		return 0;
	}
	
	// Creo un oggetto SUB e avvio la coda dei messaggi
	NZMQTSubFlight a(argc, argv);
	
	return a.exec();
}
