#ifndef NZMQTDATEUPDATE_H
#define NZMQTDATEUPDATE_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>
#include <QFile>
#include <QRegularExpressionMatch>

class NZMQTPubDateUpdate : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Funzione di inizializzazione
	void init();
	
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
public:
	// Costruttore
	NZMQTPubDateUpdate(int &argc, char **argv);
	
	// Distruttore
	~NZMQTPubDateUpdate();
};

#endif // NZMQTDATEUPDATE_H
