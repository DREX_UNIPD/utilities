#include <nzmqtdateupdate.h>

#include <iostream>

using namespace std;
using namespace nzmqt;

NZMQTPubDateUpdate::NZMQTPubDateUpdate(int argc, char **argv) : QCoreApplication(argc, argv) {
	// Creo gli oggetti
	_context = createDefaultContext();
	_context->start();
	_socket = _context->createSocket(ZMQSocket::TYP_PUB, this);
	_timer = new QTimer(this);
	
	// Estraggo il tempo per il timer
	int ms;
	ms = atoi(argv[2]);
	
	// Creo il messaggio
	for (int i = 3; i < argc; i++) {
		_message += argv[i];
	}
	
	// Connetto il socket
	string connection_string = argv[1];
	string endpoint;
	switch (connection_string[0]) {
	case '@':
		endpoint = connection_string.substr(1);
		_socket->bindTo(endpoint.c_str());
		break;
	case '>':
		endpoint = connection_string.substr(1);
		_socket->connectTo(endpoint.c_str());
		break;
	default:
		_socket->bindTo(connection_string.c_str());
		break;
	}
	
	// Collego il timer allo slot
	QObject::connect(_timer, &QTimer::timeout, this, &NZMQTPub::timeIsOver, Qt::QueuedConnection);
	
	// Faccio partire il timer
	_timer->start(ms);
}

void NZMQTPub::timeIsOver() {
	// Invio il messaggio
	cout << "Sending message...";
	_socket->sendMessage(_message);
	cout << " message sent!" << endl;
}
