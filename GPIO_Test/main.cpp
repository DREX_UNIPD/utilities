#include <iostream>
#include <string>
#include <unistd.h>
#include "simpleGPIO.h"
using namespace std;

unsigned int GPIO = 37;   // GPIO1_28 = (1x32) + 28 = 60

int main(){
	
	cout << "Testing the GPIO Pins" << endl;
	
	// Open
	//gpio_export(GPIO);
	
	// The pin is an output
	gpio_set_dir(GPIO, OUTPUT_PIN);
	
	char ans;
	
	cout << "Please press the key [H = high, L = low, Q = quit]: ";
	//ans = getchar();
	cout << endl;
	
	bool done = false;
	
	while (!done) {
		cin >> ans;
		
		switch (ans | 0x20) {
			case 'q':
				cout << "Uscita dal programma" << endl;
				done = true;
				break;
			case 'h':
				cout << "Set GPIO" << endl;
				gpio_set_value(GPIO, HIGH);
				break;
			case 'l':
				cout << "Reset GPIO" << endl;
				gpio_set_value(GPIO, LOW);
				break;
			default:
				cout << "Tasto non riconosciuto" << endl;
				break;
		}
	}
	
	// Imposto il pin in ingresso
	gpio_set_dir(GPIO, INPUT_PIN);
	
	// Unexport the LED
	//gpio_unexport(GPIO);
	
	cout << "Finished Testing the GPIO Pin" << endl;
	
	return 0;
}
