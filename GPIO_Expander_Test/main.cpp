#include <iostream>

#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

using namespace std;

void printBits(unsigned char *buffer, const int BUFFER_SIZE) {
	// Stampo il risultato
	for (int i = 0; i < BUFFER_SIZE; i++) {
		for (int b = 7; b >= 0; b--) {
			if (buffer[i] & (1 << b)) {
				cout << "1";
			}
			else {
				cout << "0";
			}
		}
		cout << " ";
	}
	cout << endl;
}

void printHex(unsigned char *buffer, const int BUFFER_SIZE, const char *padding = "") {
	for (int i = 0; i < BUFFER_SIZE; i++) {
		cout << padding;
		cout << hex << ((buffer[i] & 0xf0) >> 4);
		cout << padding;
		cout << hex << (buffer[i] & 0x0f);
		cout << " ";
	}
	cout << endl;
}

// Reset to default
void reset(int fd) {
	unsigned char buffer[1];
}

int main() {
	int file;
	const char *device = "/dev/i2c-0";
	
	// DAC A1
	const int BUFFER_SIZE = 2;
	const unsigned char ADDRESS = 0x3E; // 0x3F, 0x70, 0x71
	
	// Writing consecutively 0x12 and 0x34 to RegReset register will reset all registers to their default values
	
	
//	// Vacuometro
//	const int BUFFER_SIZE = 4;
//	const unsigned char ADDRESS = 0x28;
	
	// Buffer
	unsigned char buffer[BUFFER_SIZE];
	
	// Apro il dispositivo
	if ((file = open(device, O_RDWR)) < 0)
		exit(1);
	
	// Imposto l'indirizzo
	if (ioctl(file, I2C_SLAVE, ADDRESS) < 0)
		exit(2);
	
//	// Invio la richiesta di lettura
//	if (read(file, buffer, BUFFER_SIZE) != BUFFER_SIZE)
//		exit(3);
//	
//	// Stampo in bit
//	printBits(buffer, BUFFER_SIZE);
//	
//	// Stampo in hex
//	printHex(buffer, BUFFER_SIZE, "   ");
	
	// Invio la richiesta di scrittura
	buffer[0] = 0x0f; // Fast mode command, Power down bit disabled, D11-D8 bit set
	buffer[1] = 0xff; // D7-D0 bit set
	if (write(file, buffer, BUFFER_SIZE) != BUFFER_SIZE) {
		exit(3);
	}
	
	// Chiudo il dispositivo
	close(file);
	
	return 0;
}

