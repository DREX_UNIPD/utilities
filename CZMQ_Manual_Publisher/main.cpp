#include <czmq.h>
#include <iostream>

#include <string>
#include <strings.h>
#include <stdint.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che gli argomenti siano corretti
	if (argc < 2) {
		cout << "Usage: ./CZMQ_Manual_Publisher <endpoint>" << endl;
		return 0;
	}
	
	// Apro il socket
	zsock_t *pub = zsock_new_pub(argv[1]);
	assert(pub);
	
	// Imposto il tempo di attesa prima di terminare l'invio dei messaggi nel buffer
	zsock_set_linger(pub, -1);
	
	// Ciclo di esecuzione
	zmsg_t *msg = nullptr;
	zframe_t *frame;
	string line;
	while (true) {
		std::getline(std::cin, line);
		
		if (line == "q") break;
		
		if (line.length()) {
			if (!msg) msg = zmsg_new();
			frame = zframe_new(line.c_str(), line.length());
			zmsg_append(msg, &frame);
		}
		else {
			zmsg_send(&msg, pub);
			msg = nullptr;
		}
	}
	
	// Chiudo il socket
	zsock_destroy(&pub);
	
	return 0;
}
