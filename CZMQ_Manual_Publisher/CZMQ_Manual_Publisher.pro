TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

LIBS += /usr/local/lib/libzmq.so
LIBS += /usr/local/lib/libczmq.so
