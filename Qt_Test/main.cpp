#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>

using namespace std;

void addElement(QList<QPair<int, bool>> &list, int channel, bool flag) {
	list.append(QPair<int, bool>(channel, flag));
}

int main(int argc, char *argv[]) {
	//----------------------------------------------//
//	QDateTime x;
//	qDebug() << "Datetime test: " << x.isNull() << endl;
	//----------------------------------------------//
//	QString s = "Ciao";
//	qDebug() << "String comparison test: " << (s == "Ciao") << endl;
	//----------------------------------------------//
//	QList<QPair<int, bool>> list;
//	addElement(list, 1, true);
//	addElement(list, 2, false);
//	addElement(list, 3, false);
//	for (int i = 0; i < list.count(); i++) {
//		const QPair<int, bool> &current = list.at(i);
//		qDebug() << "Element #" << QString::number(i) << " = " << current.first << endl;
//	}
	//----------------------------------------------//
//	QString destination = "/home/ubuntu/Test/This/Is/A/Test.file";
//	QDir::root().mkpath(QFileInfo(destination).absoluteDir().absolutePath());
	//----------------------------------------------//
//	QDateTime date = QDateTime::currentDateTime();
//	qDebug() << date.toString("yyyy-MM-dd HH:mm:ss") << endl;
//	int i = 0;
//	QString video_file;
//	video_file = QDir::home().filePath(date.toString("yyyy-MM-dd HH:mm:ss") + QString(".avi"));
//	QFileInfo video_file_info(video_file);
//	while (video_file_info.exists()) {
//		video_file = QDir::home().filePath(date.toString("yyyy-MM-dd HH:mm:ss ") + QString::number(i) + QString(".avi"));
//		video_file_info.setFile(video_file);
//		i++;
//	}
//	QFile::rename(QDir::home().absoluteFilePath("test.avi"), video_file_info.absoluteFilePath());
	//----------------------------------------------//
//	QDir folder = QDir::home();
//	QString relative_path = "Test/AnotherTest";
//	if (!folder.cd(relative_path)) {
//		folder.mkpath(relative_path);
//	}
	//----------------------------------------------//
//	// Elimino i file di trigger
//	QStringList trigger_files;
//	QStringList name_filters;
//	QFileInfo trigger;
//	trigger.setFile(QDir::home(), "Cartella/esempio");
//	QDir trigger_dir;
//	trigger_dir.setPath(trigger.absolutePath());
//	name_filters += (trigger.fileName() + QString("*"));
//	trigger_files = trigger_dir.entryList(name_filters);
//	for (int i = 0; i < trigger_files.count(); i++) {
//		QFileInfo current_trigger;
//		current_trigger.setFile(trigger_dir, trigger_files[i]);
//		QString current_trigger_path;
//		current_trigger_path = current_trigger.absoluteFilePath();
//		QFile::remove(current_trigger_path);
//	}
	//----------------------------------------------//
	QString filename="/home/ubuntu/Test.txt";
	QFile file(filename);
	if (file.open(QIODevice::ReadWrite)) {
		file.close();
	}
	//----------------------------------------------//
	return 0;
}
