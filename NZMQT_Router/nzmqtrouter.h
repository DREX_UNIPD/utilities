#ifndef NZMQTROUTER_H
#define NZMQTROUTER_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>

class NZMQTRouter : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
	// Frames del messaggio di risposta
	std::vector<std::string> _frames;
	
	// Formattazione del messaggio
	QString formatMessage(const Message &msg);
	
public:
	// Costruttore
	NZMQTRouter(int argc, char **argv);
	
private slots:
	void recvMessage(const Message &msg);
};

#endif // NZMQTROUTER_H
