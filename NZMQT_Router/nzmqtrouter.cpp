#include <nzmqtrouter.h>
#include <QDateTime>
#include <iostream>

using namespace std;
using namespace nzmqt;

NZMQTRouter::NZMQTRouter(int argc, char **argv) : QCoreApplication(argc, argv) {
	// Creo gli oggetti
	_context = createDefaultContext();
	_context->start();
	_socket = _context->createSocket(ZMQSocket::TYP_ROUTER, this);
	
	// Connetto il socket
	string connection_string = argv[1];
	string endpoint;
	switch (connection_string[0]) {
	case '@':
		endpoint = connection_string.substr(1);
		_socket->bindTo(endpoint.c_str());
		break;
	case '>':
		endpoint = connection_string.substr(1);
		_socket->connectTo(endpoint.c_str());
		break;
	default:
		_socket->bindTo(connection_string.c_str());
		break;
	}
	
	if (argc < 3) {
		_frames.push_back(string("OK"));
	}
	for (int i = 2; i < argc; i++) {
		_frames.push_back(string(argv[i]));
	}
	
	// Collego il socket allo slot
	QObject::connect(_socket, &ZMQSocket::messageReceived, this, &NZMQTRouter::recvMessage, Qt::QueuedConnection);
}

void NZMQTRouter::recvMessage(const Message &msg) {
	// Stampo tutti i campi del messaggio
	QByteArray a;
	a = msg[0];
	cout << "Sender address: 0x" << a.toHex().toStdString() << endl;
	for (int i = 1; i < msg.length(); i++) {
		a = msg[i];
		cout << "Frame #" << i << ": " << a.toStdString() << endl;
	}
	
	Message ans;
	ans += msg[0];
	for (size_t i = 0; i < _frames.size(); i++) {
		ans += _frames[i].c_str();
	}
	_socket->sendMessage(ans);
}
