#include <iostream>

#include <nzmqtrouter.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 3) {
		cout << "Usage: ./NZMQT_Router <[>@]tcp://address:port> <frames ...>" << endl;
		return 0;
	}
	
	// Creo un oggetto SUB e avvio la coda dei messaggi
	NZMQTRouter a(argc, argv);
	
	return a.exec();
}
