#include <iostream>

#include <nzmqtdealer.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 3) {
		cout << "Usage: ./NZMQT_Dealer <[>@]tcp://address:port> <frame ...>" << endl;
		return 0;
	}
	
	// Creo un oggetto DEALER e avvio la coda dei messaggi
	NZMQTDealer a(argc, argv);
	
	return a.exec();
}
