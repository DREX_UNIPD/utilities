QT += core
QT -= gui

CONFIG += c++11

TARGET = NZMQT_Dealer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
	nzmqtdealer.cpp

INCLUDEPATH += \
	../ \
	../nzmqt/include

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

HEADERS += \
	nzmqtdealer.h \
	../nzmqt/include/nzmqt/nzmqt.hpp

