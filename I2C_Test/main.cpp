#include <iostream>

#include <QtCore>
#include <QRegularExpression>
#include <functional>

#include <cmd_header.h>

using namespace std;

// Variabili
QMap<QString, QString> variables;
int file;

int main(int argc, char **argv) {
	// Verifico se è stato passato per argomento il dispositivo da aprire
	if (argc < 2) {
		cout << "Usage: I2C_Test <instructions>" << endl;
		return EXIT_SUCCESS;
	}
	
	// Apro il file di istruzioni
	QFile istr(argv[1]);
	
	if (!istr.open(QIODevice::ReadOnly)) {
		cout << "Unable to open instructions file" << endl;
		return EXIT_FAILURE;
	}
	
	// Collego i comandi
	QMap<QString, std::function<bool(QRegularExpressionMatch &)>> commands;
	commands.insert(CMD_OPEN, &cmdOpen);
	commands.insert(CMD_CLOSE, &cmdClose);
	commands.insert(CMD_READ, &cmdRead);
	commands.insert(CMD_READVAR, &cmdReadVar);
	commands.insert(CMD_WRITE, &cmdWrite);
	commands.insert(CMD_WRITEVAR, &cmdWriteVar);
	commands.insert(CMD_PRINT, &cmdPrint);
	commands.insert(CMD_SELECT, &cmdSelect);
	commands.insert(CMD_EXIT, &cmdExit);
	commands.insert(CMD_COMMENT, [] (QRegularExpressionMatch &) { return true; });
	
	QMap<QString, QString> regexes;
	regexes.insert(CMD_OPEN, REGEX_OPEN);
	regexes.insert(CMD_CLOSE, REGEX_CLOSE);
	regexes.insert(CMD_READ, REGEX_READ);
	regexes.insert(CMD_READVAR, REGEX_READVAR);
	regexes.insert(CMD_WRITE, REGEX_WRITE);
	regexes.insert(CMD_WRITEVAR, REGEX_WRITEVAR);
	regexes.insert(CMD_PRINT, REGEX_PRINT);
	regexes.insert(CMD_SELECT, REGEX_SELECT);
	regexes.insert(CMD_EXIT, REGEX_EXIT);
	regexes.insert(CMD_COMMENT, REGEX_COMMENT);
	
	QTextStream stream(&istr);
	QString line;
	bool exit_flag = false;
	while (!stream.atEnd() && !exit_flag) {
		// Leggo l'istruzione
		line = stream.readLine();
<<<<<<< HEAD
	
=======
		
>>>>>>> 9f27cfbb0888a13800ea5cb80557dd82111001d8
		// Eseguo l'istruzione
		for (QString cmd : regexes.keys()) {
			QRegularExpression rx_command(regexes[cmd]); //, QRegularExpression::CaseInsensitiveOption);
			QRegularExpressionMatch match = rx_command.match(line);
			if (match.hasMatch()) {
				cout << line.toStdString() << endl;
				exit_flag = !commands[cmd](match);
				break;
			}
		}
	}
	
	return EXIT_SUCCESS;
}
