#include <cmd_header.h>

//# WRITEVAR <register> <variable>

using namespace std;

bool cmdWriteVar(QRegularExpressionMatch &match) {
	QString _register;
	QString _variable;
    QString _value;

	_register = match.captured("register");
	_variable = match.captured("variable");

    if (!variables.contains(_variable)) {
        cerr << "Variable not found" << endl;
        return false;
    }

    _value = variables[_variable];

    __s32 res = i2c_smbus_write_byte_data(file, (__u8) _register.toInt(0, 16), (__u8) _value.toInt(0, 16));
    if (res < 0) {
        /* ERROR HANDLING: i2c transaction failed */
        cerr << "Writing error" << endl;
        return false;
    }

    return true;
}
