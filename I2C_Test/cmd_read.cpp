#include <cmd_header.h>

//# READ <register>

using namespace std;

bool cmdRead(QRegularExpressionMatch &match) {
	QString _register;
	_register = match.captured("register");

	__s32 res = i2c_smbus_read_byte_data(file, (__u8) _register.toInt(0, 16));
	if (res < 0) {
		/* ERROR HANDLING: i2c transaction failed */
		cerr << "Reading error" << endl;
		return false;
	}

	QString s;
	s = "\033[31m0x";
	s += QString::number((__u8) res, 16).toLower().rightJustified(2, '0');
	s += "\033[37m";
	cout << s.toStdString() << endl;

	return true;
}
