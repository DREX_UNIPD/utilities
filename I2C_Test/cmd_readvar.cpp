#include <cmd_header.h>

//# READ <register> <variable>

using namespace std;

bool cmdReadVar(QRegularExpressionMatch &match) {
	QString _register;
	QString _variable;

	_register = match.captured("register");
	_variable = match.captured("variable");

	__s32 res = i2c_smbus_read_byte_data(file, (__u8) _register.toInt(0, 16));
	if (res < 0) {
		/* ERROR HANDLING: i2c transaction failed */
		cerr << "Reading error" << endl;
		return false;
	}

    QString _value;
	_value = "0x";
    _value += QString::number((__u8) res, 16).toLower().rightJustified(2, '0');

	// Salvo il valore nella variabile
	if (!variables.contains(_variable)) {
        variables.insert(_variable, _value);
	}
	else {
        variables[_variable] = _value;
	}

	return true;
}
