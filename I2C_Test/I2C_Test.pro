TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

SOURCES += main.cpp \
    cmd_open.cpp \
    cmd_close.cpp \
    cmd_select.cpp \
    cmd_exit.cpp \
    cmd_read.cpp \
    cmd_write.cpp \
    cmd_print.cpp \
    constants.cpp \
    cmd_writevar.cpp \
    cmd_readvar.cpp

DISTFILES += \
    instructions.txt

HEADERS += \
    cmd_header.h

