#include <cmd_header.h>

const QString CMD_OPEN = "OPEN";
const QString CMD_CLOSE = "CLOSE";
const QString CMD_SELECT = "SELECT";
const QString CMD_READ = "READ";
const QString CMD_READVAR = "READVAR";
const QString CMD_PRINT = "PRINT";
const QString CMD_WRITE = "WRITE";
const QString CMD_WRITEVAR = "WRITEVAR";
const QString CMD_EXIT = "EXIT";
const QString CMD_COMMENT = "COMMENT";

const QString REGEX_OPEN = "(?i)^ *(?<cmd>OPEN) +(?<device>(?:/(?:\\w|-|\\_)+)+) *$";
const QString REGEX_CLOSE = "(?i)^ *(?<cmd>CLOSE) *$";
const QString REGEX_SELECT = "(?i)^ *(?<cmd>SELECT) +(?<address>0x(?:[0-9a-f]{1,2})) *$";
const QString REGEX_READ = "(?i)^ *(?<cmd>READ) +(?<register>0x(?:[0-9a-f]{1,2})) *$";
const QString REGEX_READVAR = "(?i)^ *(?<cmd>READVAR) +(?<register>0x(?:[0-9a-f]{1,2})) +(?<variable>\\w+) *$";
const QString REGEX_PRINT = "(?i)^ *(?<cmd>PRINT) +(?<variable>\\w+) *$";
const QString REGEX_WRITE = "(?i)^ *(?<cmd>WRITE) +(?<register>0x(?:[0-9a-f]{1,2})) +(?<value>0x(?:[0-9a-f]{1,2})) *$";
const QString REGEX_WRITEVAR = "(?i)^ *(?<cmd>WRITEVAR) +(?<register>0x(?:[0-9a-f]{1,2})) +(?<variable>\\w+) *$";
const QString REGEX_EXIT = "(?i)^ *(?<cmd>EXIT) *$";
const QString REGEX_COMMENT = "^ *(?:#.*)?$";
