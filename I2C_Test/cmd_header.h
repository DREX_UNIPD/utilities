#ifndef CMD_HEADER
#define CMD_HEADER

#include <QtCore>
#include <iostream>
#include <linux/i2c-dev.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>

//# OPEN <device>
//# CLOSE
//# SELECT <address>
//# READ <register>
//# READVAR <register> <variable>
//# PRINT <variable>
//# WRITE <register> <value>
//# WRITEVAR <register> <variable>
//# EXIT

// Costanti
extern const QString CMD_OPEN;
extern const QString CMD_CLOSE;
extern const QString CMD_SELECT;
extern const QString CMD_READ;
extern const QString CMD_READVAR;
extern const QString CMD_PRINT;
extern const QString CMD_WRITE;
extern const QString CMD_WRITEVAR;
extern const QString CMD_EXIT;
extern const QString CMD_COMMENT;

extern const QString REGEX_OPEN;
extern const QString REGEX_CLOSE;
extern const QString REGEX_SELECT;
extern const QString REGEX_READ;
extern const QString REGEX_READVAR;
extern const QString REGEX_PRINT;
extern const QString REGEX_WRITE;
extern const QString REGEX_WRITEVAR;
extern const QString REGEX_EXIT;
extern const QString REGEX_COMMENT;

// Comandi
bool cmdOpen(QRegularExpressionMatch &match);
bool cmdClose(QRegularExpressionMatch &match);
bool cmdSelect(QRegularExpressionMatch &match);
bool cmdRead(QRegularExpressionMatch &match);
bool cmdReadVar(QRegularExpressionMatch &match);
bool cmdPrint(QRegularExpressionMatch &match);
bool cmdWrite(QRegularExpressionMatch &match);
bool cmdWriteVar(QRegularExpressionMatch &match);
bool cmdExit(QRegularExpressionMatch &match);

extern QMap<QString, QString> variables;
//extern QString device;
//extern QString address;
extern int file;

#endif // CMD_HEADER

