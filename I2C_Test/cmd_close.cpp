#include <cmd_header.h>

//# CLOSE

using namespace std;

bool cmdClose(QRegularExpressionMatch &) {
	// Chiudo il dispositivo
	if (close(file) < 0) {
		cerr << "Device closing failed" << endl;
		return false;
	}

	return true;
}
