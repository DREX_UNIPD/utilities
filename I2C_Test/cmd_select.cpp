#include <cmd_header.h>

//# SELECT <address>

using namespace std;

bool cmdSelect(QRegularExpressionMatch &match) {
	QString _address;
	_address = match.captured("address");

	// Seleziono il dispositivo con cui voglio comunicare
	if (ioctl(file, I2C_SLAVE, _address.toInt(0, 16)) < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		cerr << "Address selection error" << endl;
		return false;
	}

	return true;
}

