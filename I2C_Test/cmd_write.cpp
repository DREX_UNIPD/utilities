#include <cmd_header.h>

//# WRITE <register> <value>

using namespace std;

bool cmdWrite(QRegularExpressionMatch &match) {
	QString _register;
	QString _value;

	_register = match.captured("register");
	_value = match.captured("value");

    __s32 res = i2c_smbus_write_byte_data(file, (__u8) _register.toInt(0, 16), (__u8) _value.toInt(0, 16));
    if (res < 0) {
        /* ERROR HANDLING: i2c transaction failed */
        cerr << "Writing error" << endl;
        return false;
    }

	return true;
}

