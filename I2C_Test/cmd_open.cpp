#include <cmd_header.h>

//# OPEN <device>

using namespace std;

bool cmdOpen(QRegularExpressionMatch &match) {
	QString _device;
	_device = match.captured("device");

	// Apro il dispositivo
	if ((file = open(_device.toStdString().c_str(), O_RDWR)) < 0) {
		/* ERROR HANDLING: you can check errno to see what went wrong */
		cerr << "Failed to open the i2c bus" << endl;
		return false;
	}

	return true;
}
