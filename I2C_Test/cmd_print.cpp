#include <cmd_header.h>

//# PRINT <variable>

using namespace std;

bool cmdPrint(QRegularExpressionMatch &match) {
	QString _variable;
	_variable = match.captured("variable");

	if (!variables.contains(_variable)) {
		cout << "Variable \"" << _variable.toStdString() << "\" not found" << endl;
		return false;
	}

	cout << "\033[31m";
	cout << _variable.toStdString() << " = " << variables[_variable].toStdString() << endl;
	cout << "\033[37m";
	return true;
}

