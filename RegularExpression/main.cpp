#include "regularexpressiondialog.h"

#include <QApplication>

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	RegularExpressionDialog dialog;
	dialog.show();
	return app.exec();
}
