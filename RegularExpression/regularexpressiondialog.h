#ifndef REGULAREXPRESSIONDIALOG_H
#define REGULAREXPRESSIONDIALOG_H

#include <QDialog>

class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QSpinBox;
class QPlainTextEdit;
class QTreeWidget;

class RegularExpressionDialog : public QDialog
{
    Q_OBJECT

public:
    RegularExpressionDialog(QWidget *parent = 0);

private:
    void refresh();
    void copyEscapedPatternToClipboard();
    void setupUi();
    QWidget *setupLeftUi();
    QWidget *setupRightUi();
    void setResultUiEnabled(bool enabled);

    QLineEdit *patternLineEdit;
    QLineEdit *escapedPatternLineEdit;

    QPlainTextEdit *subjectTextEdit;

    QCheckBox *caseInsensitiveOptionCheckBox;
    QCheckBox *dotMatchesEverythingOptionCheckBox;
    QCheckBox *multilineOptionCheckBox;
    QCheckBox *extendedPatternSyntaxOptionCheckBox;
    QCheckBox *invertedGreedinessOptionCheckBox;
    QCheckBox *dontCaptureOptionCheckBox;
    QCheckBox *useUnicodePropertiesOptionCheckBox;
    QCheckBox *optimizeOnFirstUsageOptionCheckBox;
    QCheckBox *dontAutomaticallyOptimizeOptionCheckBox;

    QSpinBox *offsetSpinBox;

    QComboBox *matchTypeComboBox;

    QCheckBox *anchoredMatchOptionCheckBox;
    QCheckBox *dontCheckSubjectStringMatchOptionCheckBox;

    QTreeWidget *matchDetailsTreeWidget;

    QLabel *regexpStatusLabel;
    QTreeWidget *namedGroupsTreeWidget;
};

#endif
