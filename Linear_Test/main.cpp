#include <iostream> // cout

#include <iomanip>
#include <fcntl.h> // open O_RDWR
#include <unistd.h> // close
#include <sys/stat.h>
#include <sys/ioctl.h> // ioctl
#include <string.h> // strerr
#include <linux/spi/spidev.h>

// Per misura del tempo di conversione
#include <sys/time.h>
#include <stdio.h>

#include <spiLTC2983.h>


using namespace std;

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define FOURTH_BYTE(a) ((a & 0xff000000) >> 24)
#define THIRD_BYTE(a) ((a & 0xff0000) >> 16)
#define SECOND_BYTE(a) ((a & 0xff00) >> 8)
#define FIRST_BYTE(a) (a & 0xff)

#define READ_INSTR (0x03)
#define WRITE_INSTR (0x02)

string stringToLower(string str) {
	string result;
	result = str;
	
	for (size_t i = 0; i < str.length(); i++) {
		char c;
		c = result.at(i);
		if (c >= 'A' && c <= 'Z') {
			result.at(i) = c | 0x20;
		}
	}
	
	return result;
}
string stringToUpper(string str) {
	string result;
	result = str;
	
	for (size_t i = 0; i < str.length(); i++) {
		char c;
		c = result.at(i);
		if (c >= 'a' && c <= 'z') {
			result.at(i) = c & ~0x20;
		}
	}
	
	return result;
}

bool readInstruction(string instruction, bool &read, bool &write) {
	string instr;
	instr = stringToLower(instruction);
	
	read = false;
	write = false;
	
	if (instruction.compare("read") == 0) {
		read = true;
		return true;
	}
	if (instruction.compare("write") == 0) {
		write = true;
		return true;
	}
	
	return false;
}
bool readAddress(string address, int &number) {
	string s = stringToLower(address);
	
	size_t cifre;
	number = 0;
	
	// Verifico se l'indirizzo è esadecimale
	if (s[1] == 'x') {
		number = stoi(s, &cifre, 16);
		
		// Verifico che sia un indirizzo valido
		if (number > 0xffff || number < 0) {
			return false;
		}
		
		// Fine
		return true;
	}
	
	number = stoi(s, &cifre, 10);
	return true;
}

bool spiTransfer(
		int device,
		__u8 bits_per_word,
		__u16 delay_usec,
		__u32 len,
		__u16 pad,
		__u8 *rx_buf,
//		__u8 rx_nbits,
		__u32 speed_hz,
		__u8 *tx_buf,
//		__u8 tx_nbits,
		__u8 cs_change = 0
		) {
	int ret;
	
	spi_ioc_transfer tr;
	tr.bits_per_word = bits_per_word;
	tr.cs_change = cs_change;
	tr.delay_usecs = delay_usec;
	tr.len = len;
	tr.pad = pad;
	tr.rx_buf = (__u64) rx_buf;
//	tr.rx_nbits = rx_nbits;
	tr.speed_hz = speed_hz;
	tr.tx_buf = (__u64) tx_buf;
//	tr.tx_nbits = tx_nbits;
	
	ret = ioctl(device, SPI_IOC_MESSAGE(1), &tr);
	
	if (ret <= 0) {
		return false;
	}
	
	return true;
}

static const char *device = "/dev/spidev0.0";
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay = 0;

void printUsage() {
	cout << "Usage:" << endl;
	cout << "(Read) ./Linear_Test <r8/r32> <address 16 bit>" << endl;
	cout << "(Write) ./Linear_Test <w8/w32> <address 16 bit> <value>" << endl;
}

int main() {
	LTC_SPI_init(0, 0);
	LTC_reg_write(0xF0, (uint8_t)(TEMP_UNIT__C | REJECTION__50_60_HZ));
	LTC_reg_write(0xFF, (uint8_t)(0));										// set 0ms mux delay between conversions
	LTC_reg_write(MULCONV_REG, (uint32_t)(0));								// initialize multiple conversion mask register
	
	// Sense resistor
	uint32_t chdata;
	chdata = 0;
	
	// Channel 2: assign SENSE RESISTOR = 10 kOhm
	chdata =
		(uint32_t) SENSOR_TYPE__SENSE_RESISTOR |
		(uint32_t) (10000.0 * 1024.0)
	;
	LTC_ch_config(2, chdata);
	
	// Channel 3: assign RTD = PT1000 con RSense CH1-2
	// Channel 4: assign RTD = PT1000 con RSense CH1-2
	chdata = 0x78854000;
	LTC_ch_config(3, chdata);
	LTC_ch_config(5, chdata);
	LTC_ch_config(7, chdata);
	LTC_ch_config(9, chdata);
	LTC_ch_config(11, chdata);
	LTC_ch_config(13, chdata);
	LTC_ch_config(15, chdata);
	LTC_ch_config(17, chdata);
	
	// Channel 19: voltage reading
	chdata =
		(uint32_t) SENSOR_TYPE__DIRECT_ADC |
		(uint32_t) DIRECT_ADC_SINGLE_ENDED
	;
	LTC_ch_config(19, chdata);
	
	LTC_ch_add(3);
	LTC_ch_add(5);
	LTC_ch_add(7);
	LTC_ch_add(9);
	LTC_ch_add(11);
	LTC_ch_add(13);
	LTC_ch_add(15);
	LTC_ch_add(17);
	LTC_ch_add(19);

	// Converto i canali
	timeval start, end;
	gettimeofday(&start, NULL);
	LTC_mul_convert();
	gettimeofday(&end, NULL);
	cout << "Tempo trascorso: " << end.tv_sec - start.tv_sec << " s, " << end.tv_usec - start.tv_usec << " us" << endl;
	
	float t;
	t = LTC_temperature_read(3);
	cout << "Temperature ch 3 read: " << t << endl;
	t = LTC_temperature_read(5);
	cout << "Temperature ch 5 read: " << t << endl;
	t = LTC_voltage_read(19);
	cout << "Voltage ch 19 read: " << t * 11.0 * 1.0393 << endl;

	LTC_SPI_close();
	
	return 0;
}

int main4() {
	LTC_SPI_init(0, 0);
	LTC_reg_write(0xF0, (uint8_t)(TEMP_UNIT__C | REJECTION__50_60_HZ));
	//LTC_reg_write(0xFF, (uint8_t)(0));										// set 0ms mux delay between conversions
	//LTC_reg_write(MULCONV_REG, (uint32_t)(0));								// initialize multiple conversion mask register
	
	// Channel 10: assign Direct ADC
	uint32_t chdata;
	chdata =
		(uint32_t) SENSOR_TYPE__DIRECT_ADC |
		(uint32_t) DIRECT_ADC_SINGLE_ENDED
	;
	LTC_ch_config(10, chdata);
	LTC_ch_convert(10);
	
	float v;
	v = LTC_voltage_read(10);
	cout << "Voltage read: " << v << endl;
	LTC_SPI_close();
	
	return 0;
}

int main2(int argc, char **argv) {
	if (argc < 3) {
		printUsage();
		return 0;
	}
	
	// Apro la SPI
	if (!LTC_SPI_init(0, 0)) {
		cout << "SPI opening failed" << endl;
		return -1;
	}
	
	int16_t address;
	address = stoi(argv[2], 0, 16);
	int nbits;
	nbits = atoi(&argv[1][1]);
	
	switch (argv[1][0] | 0x20) {
		case 'r': {
			switch (nbits) {
				case 8: {
						uint8_t value;
						LTC_reg_read(address, value);
						cout << "Result: 0x" << std::setfill('0') << std::hex << std::setw(2) << (int) value << endl;
					}
					break;
				case 32: {
						uint32_t value;
						LTC_reg_read(address, value);
						cout << "Result: 0x" << std::setfill('0') << std::setw(8) << std::hex << value << endl;
					}
					break;
				default:
					printUsage();
					return -1;
					break;
			}
		}
		break;
		case 'w': {
			if (argc != 4) {
				printUsage();
				return -1;
			}
			
			uint32_t raw_value;
			raw_value = atoi(argv[3]);
			switch (nbits) {
				case 8: {
						uint8_t value;
						value = (uint8_t) raw_value;
						LTC_reg_write(address, value);
					}
					break;
				case 32: {
						LTC_reg_read(address, raw_value);
					}
					break;
				default:
					printUsage();
					return -1;
					break;
			}
		}
		break;
		default:
			printUsage();
			return -1;
			break;
	}
	
	// Apro la SPI
	if (!LTC_SPI_close()) {
		cout << "SPI closing failed" << endl;
		return -1;
	}
	
	return 0;
}

int main3(int argc, char **argv) {
	if (argc < 3) {
		cout << "Usage: ./Linear_Test <read/write> <address 16 bit> <bytes to send...>" << endl;
		return 0;
	}
	
	// Estraggo l'istruzione
	bool read = false;
	bool write = false;
	
	if (!readInstruction(argv[1], read, write)) {
		cout << "Bad instruction" << endl;
		return -1;
	}
	
	// Estraggo l'indirizzo
	int address;
	if (!readAddress(argv[2], address)) {
		cout << "Bad address" << endl;
		return -2;
	}
	
	// Variabili per la comunicazione
	int deviceHandle;
	deviceHandle = open(device, O_RDWR);
	if (deviceHandle < 0) {
		cout << "Can't' open device!" << endl;
		cout <<  strerror(errno) << endl;
		return -3;
	}
	cout << "SPI open" << endl;
	
	// Calcolo i byte da leggere o scrivere
	int bytes_to_manage = argc - 3;
	
	// Costruisco la richiesta
	__u8 *request = new __u8[bytes_to_manage + 3];
	
	// Leggo il dato
	if (read) {
		cout << "Reading from " << to_string(address) << endl;
		request[0] = READ_INSTR;
		
		// TEST-TEST-TEST-TEST
		for (int i = 0; i < bytes_to_manage; i++) {
			string value;
			value = argv[3 + i];
			value = stringToLower(value);
			if (value[1] == 'x') {
				request[3 + i] = (__u8) stoi(value, 0, 16);
			}
			else {
				request[3 + i] = (__u8) stoi(value, 0, 10);
			}
		}
		// TEST-TEST-TEST-TEST
	}
	
	// Scrivo il dato
	if (write) {
		cout << "Writing to " << to_string(address) << endl;
		request[0] = WRITE_INSTR;
		for (int i = 0; i < bytes_to_manage; i++) {
			string value;
			value = argv[3 + i];
			value = stringToLower(value);
			if (value[1] == 'x') {
				request[3 + i] = (__u8) stoi(value, 0, 16);
			}
			else {
				request[3 + i] = (__u8) stoi(value, 0, 10);
			}
		}
	}
	
	request[1] = SECOND_BYTE(address);
	request[2] = FIRST_BYTE(address);
	
	__u8 *answer = new __u8[bytes_to_manage + 3];
	
	if (!spiTransfer(deviceHandle, bits, delay, bytes_to_manage + 3, 0, answer, speed, request, 0)) {
		cout << "Failed to read the device" << endl;
	}
	else {
		if (read) {
			cout << "Answer: ";
			for (int i = 0; i < bytes_to_manage; i++) {
				cout << std::hex << ((int) answer[i + 3]) << " ";
			}
			cout << endl;
		}
		
		if (write) {
			cout << "Write done" << endl;
		}
	}
	
	delete[] request;
	delete[] answer;
	close(deviceHandle);
	cout << "SPI closed" << endl;
	
	return 0;
}

