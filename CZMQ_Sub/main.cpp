#include <czmq.h>
#include <iostream>

#include <string>
#include <strings.h>
#include <stdint.h>

using namespace std;

int main(int argc, char **argv) {
	// Verifico se sono presenti i parametri richiesti
	if (argc < 3) {
		cout << "Usage: subscriber <endpoint> <filter>" << endl;
		return 0;
	}
	
	// Creo un nuovo socket
	zsock_t *sub = zsock_new_sub(argv[1], argv[2]);
	assert(sub);
	
	// Imposto il tempo di attesa prima di terminare l'invio dei messaggi nel buffer
	zsock_set_linger(sub, -1);
	
	// Ricevo un messaggio, lo visualizzo e lo distruggo
	zmsg_t *msg;
	while (true) {
		msg = zmsg_recv(sub);
		if (!msg) break;
		cout << "Message received\n" << endl;
		zmsg_print(msg);
		zmsg_destroy(&msg);
	}
	
	// Distruggo il socket
	zsock_destroy(&sub);
	
	return 0;
}
