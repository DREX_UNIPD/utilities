#include <iostream>
#include <yaml-cpp/yaml.h>
#include <yaml-cpp/parser.h>
#include <QtCore>

using namespace std;

// Carico la configurazione in base ai parametri forniti
bool loadConfiguration(QStringList &paths, YAML::Node &destination) {
	QString file_content;
	
	for (int i = 0; i < paths.count(); i++) {
		QString path = paths[i];
		
		QFileInfo check_file(path);
		
		// Verifico che il file esista
		if (!check_file.exists()) {
			cout << "YAML file \"" + path.toStdString() + "\" don't exists!" << endl;
			return false;
		}
		
		if (!check_file.isFile()) {
			cout << "YAML file \"" + path.toStdString() + "\" is not a file!" << endl;
			return false;
		}
		
		// Sostituisco le tabulazioni con spazi altrimenti la libreria
		// yaml-cpp crasha
		QFile input_file(path);
		if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			cout << "Configuration file opening failed!" << endl;
			return false;
		}
		file_content += QTextStream(&input_file).readAll();
	}
	
	file_content = file_content.replace(QRegularExpression("\\t"), QString("    "));
	
	// Carico la configurazione
	destination = YAML::Load(file_content.toStdString());
	if (!destination) {
		cout << "Invalid YAML file format" << endl;
		return false;
	}
	
	cout << "Configuration loaded" << endl;
	
	return true;
}

void recursiveExplore(const int level, const YAML::Node &node) {
	if (!node.IsSequence() && !node.IsMap()) return;
	
	string padding = std::string(level, ' ');
	
	if (node.IsSequence()) {
		for(YAML::const_iterator it = node.begin(); it != node.end(); ++it) {
			cout << padding << "-";
			
			const YAML::Node &current = *it;
			
			// Value
			switch (current.Type()) {
			case YAML::NodeType::Null:
				cout << " [Null]" << endl;
				break;
			case YAML::NodeType::Scalar: {
					string v;
					v = current.as<string>();
					cout << " " << v << " [Scalar]" << endl;
				}
				break;
			case YAML::NodeType::Sequence:
				cout << " [Sequence]" << endl;
				recursiveExplore(level + 1, current);
				break;
			case YAML::NodeType::Map:
				cout << " [Map]" << endl;
				recursiveExplore(level + 1, current);
				break;
			case YAML::NodeType::Undefined:
				cout << " [Undefined]" << endl;
				break;
			}
		}
		
		return;
	}
	
	if (node.IsMap()) {
		for(YAML::const_iterator it = node.begin(); it != node.end(); ++it) {
			cout << padding;
			
			// Key
			string key = it->first.as<std::string>();       // <- key
			
			const YAML::Node &current = node[key];
			
			// Value
			switch (current.Type()) {
			case YAML::NodeType::Null:
				cout << key << " [Null]" << endl;
				break;
			case YAML::NodeType::Scalar: {
					string v;
					v = current.as<string>();
					cout << key << ": " << v << " [Scalar]" << endl;
				}
				break;
			case YAML::NodeType::Sequence:
				cout << key << " [Sequence]" << endl;
				recursiveExplore(level + 1, current);
				break;
			case YAML::NodeType::Map:
				cout << key << " [Map]" << endl;
				recursiveExplore(level + 1, current);
				break;
			case YAML::NodeType::Undefined:
				cout << key << " [Undefined]" << endl;
				break;
			}
		}
		
		return;
	}
}

int main(int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: ./YAML_Explorer [<filepath.yaml>, ...]" << endl;
		return 0;
	}
	
	YAML::Node node;
	QStringList paths;
	
	for (int i = 1; i < argc; i++) paths.append(argv[i]);
	
	if (!loadConfiguration(paths, node)) {
		cout << "Failed to load YAML files" << endl;
		return -1;
	}
	
	// Type
	switch (node.Type()) {
	case YAML::NodeType::Null:
		cout << "Node is [Null]" << endl;
		break;
	case YAML::NodeType::Scalar:
		cout << "Node is [Scalar]" << endl;
		break;
	case YAML::NodeType::Sequence:
		cout << "Node is [Sequence]" << endl;
		break;
	case YAML::NodeType::Map:
		cout << "Node is [Map]" << endl;
		break;
	case YAML::NodeType::Undefined:
		cout << "Node is [Undefined]" << endl;
		break;
	}
	
	cout << "-------------" << endl;
	
	recursiveExplore(0, node);
	
	cout << "Bye bye" << endl;
	
	return 0;
}

