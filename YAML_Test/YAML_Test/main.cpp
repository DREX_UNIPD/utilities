// Header Qt
#include <QFileInfo>
#include <QTextStream>
#include <QRegularExpression>

// Header STL
#include <iostream>

// Header YAML
#include <yaml-cpp/yaml.h>

using namespace std;

// Carico la configurazione in base ai parametri forniti
bool loadConfiguration(QString &path, YAML::Node &destination) {
	QFileInfo check_file(path);
	
	// Verifico che il file esista
	if (!check_file.exists()) {
		cout << "Config file \"" + path.toStdString() + "\" don't exists!" << endl;
		return false;
	}
	
	if (!check_file.isFile()) {
		cout << "Config file \"" + path.toStdString() + "\" is not a file!" << endl;
		return false;
	}
	
	// Sostituisco le tabulazioni con spazi altrimenti la libreria
	// yaml-cpp crasha
	QFile input_file(path);
	if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		cout << "Configuration file opening failed!" << endl;
		return false;
	}
	QString file_content = QTextStream(&input_file).readAll();
	file_content = file_content.replace(QRegularExpression("\\t"), QString("    "));
	
	// Carico la configurazione
	destination = YAML::Load(file_content.toStdString());
	if (!destination) {
		cout << "Invalid YAML file format" << endl;
		return false;
	}
	
	cout << "Configuration loaded" << endl;
	
	return true;
}

void recursiveRead(const YAML::Node &node, string padding) {
	// Stampo il nodo
	cout << padding << "Tag: " << node.Tag() << endl;
	
	for (YAML::const_iterator it = node.begin(); it != node.end(); ++it) {
		const YAML::Node& subnode = *it;
		recursiveRead(subnode, padding + "--");
	}
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		cout << "Usage: ./YAML_Test <filepath.yaml>" << endl;
		return 0;
	}
	
	// Carico la configurazione
	YAML::Node node;
	QString path(argv[1]);
	
	if (!loadConfiguration(path, node)) {
		cout << "Non va una madonna!" << endl;
		return 0;
	}
	
	//recursiveRead(node, "");
	//cout << "Sockets - SensorsDataLocal - IP: " << node["Sockets"]["SensorsDataLocal"]["IP"].as<string>() << endl;
	//cout << "Miao - Bau: " << node["Miao"]["Bau"].as<string>() << endl;
	//cout << "Miao - Quaqua: " << node["Miao"]["Quaqua"].as<string>() << endl;
	
	string home;
	string process;
	
	process = node["Storage"]["Process"].as<string>();
	cout << "Process: \"" << process << "\"" << endl;
	
	home = node["Storage"]["Home"].as<string>();
	cout << "Home: \"" << home << "\"" << endl;
	
	return 0;
}
