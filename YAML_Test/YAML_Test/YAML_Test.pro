# Qt
QT += core
QT -= gui

# Config
CONFIG -= app_bundle
CONFIG += c++11
CONFIG += console
CONFIG += thread

# File sorgente
SOURCES += main.cpp

# Librerie
LIBS += /usr/local/lib/libyaml-cpp.a
