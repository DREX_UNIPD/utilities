#include <iostream>
#include <yaml-cpp/yaml.h>
#include <QtCore>

using namespace std;

// Carico la configurazione in base ai parametri forniti
bool loadConfiguration(QString common, QString specific, YAML::Node &destination) {
	QFileInfo check_common_file(common), check_specific_file(specific);
	QFile input_file;
	QString file_content;
	
	file_content.clear();
	
	// Verifico che il file delle impostazioni comuni esista
	if (check_common_file.exists() && check_common_file.isFile()) {
		input_file.setFileName(common);
		
		if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			cout << "Common configuration file opening failed!" << endl;
			return false;
		}
		file_content += QTextStream(&input_file).readAll();
	}
	
	// Verifico che il file specifico del processo esista
	if (check_specific_file.exists() && check_specific_file.isFile()) {
		input_file.setFileName(specific);
		
		if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			cout << "Specific configuration file opening failed!" << endl;
			return false;
		}
		file_content += QTextStream(&input_file).readAll();
	}
	
	// Verifico che sia stato caricato qualcosa
	if (file_content.isEmpty()) {
		cout << "No configuration loaded" << endl;
		return false;
	}
	
	// Sostituisco le tabulazioni con spazi altrimenti la libreria
	// yaml-cpp crasha
	file_content = file_content.replace(QRegularExpression("\\t"), QString("    "));
	
	// Carico la configurazione
	destination = YAML::Load(file_content.toStdString());
	if (!destination) {
		cout << "Invalid YAML file format" << endl;
		return false;
	}
	
	cout << "Configuration loaded" << endl;
	
	return true;
}

int main(int argc, char **argv) {
	// Verifico che i parametri siano stati passati correttamente
	if (argc < 2) {
		cout << "Usage: ./YAML_Test2 <common file path> <specific file path>" << endl;
		return 0;
	}
	
	// Carico la configurazione
	YAML::Node node;
	if (!loadConfiguration(argv[1], argv[2], node)) {
		cout << "Failed to load configuration files" << endl;
		return -1;
	}
	
	cout << "Storage - RAM: " << node["Storage"]["RAM"].as<string>() << endl;
	cout << "Storage - Process: " << node["Storage"]["Process"].as<string>() << endl;
	
	return 0;
}

