#include <sender_agent.h>

#include <iostream>

using namespace std;

// Costruttore
SenderAgent::SenderAgent(int id, QString filepath) :
	_id(id),
	_filepath(filepath) {
}

void SenderAgent::init() {
//	cout << "(" << _id << ")" << "Init" << endl;
	
	QString &filepath = _filepath;
	
	QFileInfo check_file(filepath);
	
	// Verifico che il file esista
	if (!check_file.exists()) {
		QString txt;
		txt += "Data file \"";
		txt += filepath;
		txt += "\" don't exists!";
//		cout << "(" << _id << ")" << txt.toStdString() << endl;
		emit requestPrintText(_id, txt);
		emit requestTerminate(_id);
		return;
	}
	
	if (!check_file.isFile()) {
		QString txt;
		txt += "Data file \"";
		txt += filepath;
		txt += "\" is not a file!";
//		cout << "(" << _id << ")" << txt.toStdString() << endl;
		emit requestPrintText(_id, txt);
		emit requestTerminate(_id);
		return;
	}
	
	// Apro il file da leggere
	_file = new QFile(filepath);
	if (!_file->open(QIODevice::ReadOnly | QIODevice::Text)) {
//		cout << "(" << _id << ")" << "Data file opening failed!" << endl;
		emit requestPrintText(_id, "Data file opening failed!");
		emit requestTerminate(_id);
		return;
	}
	if (_file->atEnd()) {
//		cout << "(" << _id << ")" << "File at end" << endl;
		emit requestTerminate(_id);
		return;
	}
	
	// Leggo l'header
	QString line;
	QRegularExpression rx;
	QRegularExpressionMatch match;
	
	while (!_file->atEnd()) {
		// Leggo la prossima linea
		line = _file->readLine();
		if(line.endsWith("\n")) line.truncate(line.length() - 1);
		line = line.replace('\t', ' ');
		
		// Commento
		rx.setPattern(REGEX_COMMENT);
		match = rx.match(line);
		if (match.hasMatch()) {
			continue;
		}
		
		// Nome
		rx.setPattern(REGEX_NAME);
		match = rx.match(line);
		if (match.hasMatch()) {
			if (_signal_name.length() == 0) {
				_signal_name = match.captured("name");
				continue;
			}
			else {
//				cout << "(" << _id << ")" << "Name already set!" << endl;
				emit requestPrintText(_id, "Name already set!");
				emit requestTerminate(_id);
				return;
			}
		}
		
		// Fields number
		rx.setPattern(REGEX_FIELDS);
		match = rx.match(line);
		if (match.hasMatch()) {
			if (_fields == 0) {
				_fields = match.captured("fields").toInt();
				
				// Leggo i nomi dei campi
				for (int i = 0; i < _fields; i++) {
					if (_file->atEnd()) {
//						cout << "(" << _id << ")" << "Unexpected end of file" << endl;
						emit requestPrintText(_id, "Unexpected end of file");
						emit requestTerminate(_id);
						return;
					}
					
					line = _file->readLine();
					if(line.endsWith("\n")) line.truncate(line.length() - 1);
					line = line.replace('\t', ' ');
					
					// Verifico la sintassi
					_rx_line.setPattern(REGEX_FIELD);
					QRegularExpressionMatch match_field;
					match_field = _rx_line.match(line);
					
					if (match_field.hasMatch()) {
						_fields_names.append(match_field.captured("name").toLower());
						_fields_types.append(match_field.captured("type").toUpper());
					}
					else {
						QString txt;
						txt += "Field syntax incorrect: ";
						txt += line;
//						cout << "(" << _id << ")" << txt.toStdString() << endl;
						emit requestPrintText(_id, txt);
						emit requestTerminate(_id);
						return;
					}
				}
				
				continue;
			}
			else {
//				cout << "(" << _id << ")" << "Fields number already set!" << endl;
				emit requestPrintText(_id, "Fields number already set!");
				emit requestTerminate(_id);
				return;
			}
		}
		
		// Dati
		rx.setPattern(REGEX_DATA);
		match = rx.match(line);
		if (match.hasMatch()) {
			// Verifico che il numero di campi sia stato impostato
			if (_fields <= 0) {
//				cout << "(" << _id << ")" << "Fields number need to be set before data!" << endl;
				emit requestPrintText(_id, "Fields number need to be set before data!");
				emit requestTerminate(_id);
				return;
			}
			
			// Costruisco la regex per i dati
			QString regex_single_data;
			regex_single_data = "(?i)^ *(?<time>\\d+)";
			for (int i = 0; i < _fields; i++) {
				regex_single_data += " +(?<";
				regex_single_data += _fields_names[i];
				if (_fields_types[i] == "STRING") regex_single_data += ">(?:\\w|_)+)";
				if (_fields_types[i] == "NUMBER") regex_single_data += ">\\d+\\.?\\d*)";
			}
			regex_single_data += " *$";
			_rx_line.setPattern(regex_single_data);
			
			// Leggo il file fino alla fine memorizzando i dati validi
			bool first = true;
			QRegularExpressionMatch match_data;
			while (!_file->atEnd()) {
				line = _file->readLine();
				if(line.endsWith("\n")) line.truncate(line.length() - 1);
				line = line.replace('\t', ' ');
				match_data = _rx_line.match(line);
				if (match_data.hasMatch()) {
					_data.append(line);
					if (first) {
						_last_time = match.captured("time").toInt() * 1000;
						first = false;
					}
					continue;
				}
			}
			
			break;
		}
		
		// Ignora la riga non riconosciuta
	}
	
	// Verifico che sia stato impostato il nome del segnale
	if (_signal_name.length() == 0) {
//		cout << "(" << _id << ")" << "Error: Signal name missing" << endl;
		emit requestPrintText(_id, "Error: Signal name missing");
		emit requestTerminate(_id);
		return;
	}
	
	// Verifico che il numero di campi sia positivo e che i nomi dei campi siano stati specificati
	if (_fields <= 0) {
//		cout << "(" << _id << ")" << "Error: Field number missing" << endl;
		emit requestPrintText(_id, "Error: Field number missing");
		emit requestTerminate(_id);
		return;
	}
	if (_fields_names.length() != _fields) {
//		cout << "(" << _id << ")" << "Error: Fields names missing" << endl;
		emit requestPrintText(_id, "Error: Fields names missing");
		emit requestTerminate(_id);
		return;
	}
	
	// Verifico che ci sia almeno un dato
	if (_data.length() == 0) {
		QString txt;
		txt += _signal_name;
		txt += ": execution terminated";
//		cout << "(" << _id << ")" << txt.toStdString() << endl;
		emit requestPrintText(_id, txt);
		emit requestTerminate(_id);
		return;
	}
	
	// Creo il timer e lo collego allo slot
	_timer = new QTimer(this);
	_timer->setSingleShot(true);
	QObject::connect(_timer, &QTimer::timeout, this, &SenderAgent::executeNext, Qt::QueuedConnection);
	
//	cout << "(" << _id << ")" << "Data count: " << _data.count() << endl;
	
	_timer->start();
}

void SenderAgent::executeNext() {
//	cout << "(" << _id << ")" << "Execute next" << endl;
	
	// Leggo la prossima istruzione da eseguire, se sono finite esco
	QString line;
	
	// Verifico se ci sono ancora dati
	if (_data.length() == 0) {
		QObject::disconnect(_timer, &QTimer::timeout, this, &SenderAgent::executeNext);
		QString txt;
		txt += _signal_name;
		txt += ": execution terminated";
		emit requestPrintText(_id, txt);
		emit requestTerminate(_id);
		return;
	}
	
	// Leggo il prossimo dato e lo rimuovo dalla coda
	line = _data.first();
	_data.removeFirst();
	
	// Interpreto i dati dei campi
	QRegularExpressionMatch match;
	match = _rx_line.match(line);
	
	// Creo il messaggio da inviare
	Message msg;
	msg += _signal_name.toStdString().c_str();
	for (int i = 0; i < _fields; i++) {
		QString field;
		field = _fields_names[i];
		field += " = ";
		field += match.captured(_fields_names[i]);
		msg += field.toStdString().c_str();
	}
	emit requestSendMessage(_id, msg);
	
	// Faccio partire il timer
	int new_time;
	new_time = match.captured("time").toInt() * 1000;
	
//	cout << "(" << _id << ")" << "New time " <<  new_time << ", Last time " << _last_time << endl;
	
	if (new_time <= _last_time) {
//		cout << "(" << _id << ")" << "Here 1" << endl;
		_timer->start(0);
	}
	else {
//		cout << "(" << _id << ")" << "Here 2" << endl;
		_timer->start(new_time - _last_time);
	}
//	cout << "(" << _id << ")" << "Here 3" << endl;
	_last_time = new_time;
}

// Distruttore
SenderAgent::~SenderAgent() {
	
}
