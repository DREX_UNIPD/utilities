QT += core
QT -= gui

CONFIG += c++11

TARGET = NZMQT_Publisher_Simulator
CONFIG += console
CONFIG -= app_bundle
CONFIG += threads

TEMPLATE = app

SOURCES += main.cpp \
    nzmqtpubsim.cpp \
    sender_agent.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
	../ \
	../nzmqt/include

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

HEADERS += \
	../nzmqt/include/nzmqt/nzmqt.hpp \
	nzmqtpubsim.h \
    sender_agent.h

DISTFILES += \
    imu.sensor \
    microswitch.sensor \
    barometer-altitude.sensor \
    vacuometer.sensor \
    imu-freefall.sensor \
    fast/gpioexpander-microswitch.sensor \
    fast/imu-freefall.sensor \
    fast/vacuometer-altitude.sensor \
    slow/gpioexpander-microswitch.sensor \
    slow/imu-freefall.sensor \
    slow/vacuometer-altitude.sensor
