#ifndef SENDER_AGENT_H
#define SENDER_AGENT_H

#include <QtCore>
#include <functional>

class SenderAgent : public QObject {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// ID dell'agent
	int _id;
	
	// Costanti
	const QString REGEX_NAME = "(?i)^ *NAME +(?<name>.+) *$";
	const QString REGEX_FIELDS = "(?i)^ *FIELDS +(?<fields>\\d+) *$";
	const QString REGEX_FIELD = "(?i)^ *(?<name>(?:\\w|_)+) +(?<type>NUMBER|STRING) *$";
	const QString REGEX_DATA = "(?i)^ *DATA *$";
	const QString REGEX_COMMENT = "^ *(?:#.*)?$";
	
	// File sorgente dei dati
	QString _header_name_regex;
	QString _header_fields_regex;
	
	// Dati dal file
	QString _filepath;
	
	QString _signal_name;
	int _fields;
	QStringList _fields_names;
	QStringList _fields_types;
	QStringList _data;
	QRegularExpression _rx_line;
	
	
	// Timer
	int _last_time;
	QTimer *_timer;
	
	// File di simulazione
	QFile *_file;
	
	// Stampa colorata
	void textcolor(int attr, int fg);
	void textcolor(int attr, int fg, int bg);
	
	// Comandi
	QMap<QString, std::function<bool(QRegularExpressionMatch &)>> _commands;
	
	// Espressioni regolari
	QMap<QString, QString> _regexes;
	
public:
	// Costruttore
	SenderAgent(int id, QString filepath);
	
	// Inizio esecuzione
	void init();
	
	// Distruttore
	~SenderAgent();
	
public slots:
	// Timeout
	void executeNext();
	
signals:
	void requestSendMessage(const int id, const Message &msg);
	void requestPrintText(const int id, const QString &txt);
	void requestTerminate(const int id);
};

#endif // SENDER_AGENT_H
