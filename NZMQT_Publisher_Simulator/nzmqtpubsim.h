#ifndef NZMQTPUB_H
#define NZMQTPUB_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>
#include <QFile>
#include <QRegularExpressionMatch>
#include <sender_agent.h>

class NZMQTPubSim : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Funzione di inizializzazione
	void init();
	
	// Simulazione
	void startSimulation();
	
	// Threads
	QMap<int, QThread *> _threads;
	
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
public:
	// Costruttore
	NZMQTPubSim(int &argc, char **argv);
	
	// Distruttore
	~NZMQTPubSim();
	
private slots:
	// Servizi per gli agent
	void terminate(const int id);
	void sendMessage(const int id, const Message &msg);
	void printText(const int id, const QString &txt);
};

#endif // NZMQTPUB_H
