#include <nzmqtpubsim.h>

#include <iostream>

#include <QFileInfo>

using namespace std;
using namespace nzmqt;

#define RESET		0
#define BRIGHT 		1
#define DIM			2
#define UNDERLINE 	3
#define BLINK		4
#define REVERSE		7
#define HIDDEN		8

#define BLACK 		0
#define RED			1
#define GREEN		2
#define YELLOW		3
#define BLUE		4
#define MAGENTA		5
#define CYAN		6
#define	WHITE		7

//// Stampa colorata
//void NZMQTPubSim::textcolor(int attr, int fg) {
//	char command[13];
	
//	/* Command is the control command to the terminal */
//	sprintf(command, "%c[%d;%dm", 0x1B, attr, fg + 30);
//	cout << command;
//}

//void NZMQTPubSim::textcolor(int attr, int fg, int bg) {
//	char command[13];
	
//	/* Command is the control command to the terminal */
//	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
//	cout << command;
//}

// Costuttore
NZMQTPubSim::NZMQTPubSim(int &argc, char **argv) : QCoreApplication(argc, argv) {
	QTimer::singleShot(0, this, &NZMQTPubSim::init);
}

// Inizializzazione
void NZMQTPubSim::init() {
	QStringList args = arguments();
	
	// Creo il socket
	_context = createDefaultContext();
	_context->start();
	_socket = _context->createSocket(ZMQSocket::TYP_PUB, this);
	_socket->setLinger(-1);
	
	// Connetto il socket
	string connection_string = args[1].toStdString();
	string endpoint;
	switch (connection_string[0]) {
	case '@':
		endpoint = connection_string.substr(1);
		_socket->bindTo(endpoint.c_str());
		break;
	case '>':
		endpoint = connection_string.substr(1);
		_socket->connectTo(endpoint.c_str());
		break;
	default:
		_socket->bindTo(connection_string.c_str());
		break;
	}
	
	QTimer::singleShot(3000, this, &NZMQTPubSim::startSimulation);
}

// Inizio della simulazione
void NZMQTPubSim::startSimulation() {
	QStringList args = arguments();
	SenderAgent *sa;
	for (int i = 2; i < args.length(); i++) {
		QThread *thread;
		
		sa = new SenderAgent(i, args[i]);
		thread = new QThread();
		sa->moveToThread(thread);
		_threads.insert(i, thread);
		
		QObject::connect(sa, &SenderAgent::requestSendMessage, this, &NZMQTPubSim::sendMessage, Qt::QueuedConnection);
		QObject::connect(sa, &SenderAgent::requestPrintText, this, &NZMQTPubSim::printText, Qt::QueuedConnection);
		QObject::connect(sa, &SenderAgent::requestTerminate, this, &NZMQTPubSim::terminate, Qt::QueuedConnection);
		
		QObject::connect(thread, &QThread::started, sa, &SenderAgent::init, Qt::QueuedConnection);
		QObject::connect(sa, &SenderAgent::requestTerminate, thread, &QThread::quit, Qt::QueuedConnection);
		
		QObject::connect(sa, &SenderAgent::requestTerminate, sa, &SenderAgent::deleteLater, Qt::QueuedConnection);
		QObject::connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::QueuedConnection);
	}
	
	for (int i = 0; i < _threads.count(); i++) {
		int id;
		QThread *thread;
		
		id = _threads.keys().at(i);
		thread = _threads[id];
		thread ->start();
	}
}

// Distruttore
NZMQTPubSim::~NZMQTPubSim() {
}

// Comando di temrinazione del thread
void NZMQTPubSim::terminate(const int id) {
	if (_threads.contains(id)) {
		_threads.remove(id);
		if (_threads.empty()) {
			quit();
		}
	}
}

// Comando di invio di un messaggio
void NZMQTPubSim::sendMessage(const int id, const Message &msg) {
	cout << "(" << id << ") Message" << endl;
	// Stampo tutti i campi del messaggio
	QByteArray a;
	for (int i = 0; i < msg.length(); i++) {
		a = msg[i];
		cout << ("Frame #" + std::to_string(i) + ": " + a.toStdString()) << endl;
	}
	
	_socket->sendMessage(msg);
}

// Comando di stampa a video
void NZMQTPubSim::printText(const int, const QString &txt) {
	cout << txt.toStdString() << endl;
}
