#include <iostream>

#include <nzmqtpubsim.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 3) {
		cout << "Usage: ./NZMQT_Publisher <[>@]tcp://address:port> <simulation_file_path>" << endl;
		return 0;
	}
	
	// Creo un oggetto PUB e avvio la coda dei messaggi
	NZMQTPubSim a(argc, argv);
	
	return a.exec();
}
