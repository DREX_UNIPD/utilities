#include <czmq.h>
#include <iostream>

#include <string>
#include <strings.h>
#include <stdint.h>

using namespace std;

int main(int argc, char **argv) {
	// Verifico se sono presenti i parametri richiesti
	if (argc < 2) {
		cout << "Usage: rep <endpoint> [<frame>, ...]" << endl;
		return 0;
	}
	
	// Creo un nuovo socket
	zsock_t *rep = zsock_new_rep(argv[1]);
	assert(rep);
	
	// Imposto il tempo di attesa prima di terminare l'invio dei messaggi nel buffer
	zsock_set_linger(rep, -1);
	
	zmsg_t *msg;
	zframe_t *frame;
	while (true) {
		// Ricevo un messaggio, lo visualizzo e lo distruggo
		msg = zmsg_recv(rep);
		cout << "Message received\n" << endl;
		zmsg_print(msg);
		zmsg_destroy(&msg);
	
		// Creo un messaggio e lo compongo
		msg = zmsg_new();
		if (argc < 3) {
			frame = zframe_new("OK", 2);
			zmsg_append(msg, &frame);
		}
		else {
			for (int i = 2; i < argc; i++) {
				frame = zframe_new(argv[i], strlen(argv[i]));
				zmsg_append(msg, &frame);
			}
		}
		cout << "Message sent\n" << endl;
		zmsg_print(msg);
		zmsg_send(&msg, rep);
	}
	
	// Distruggo il socket
	zsock_destroy(&rep);
	
	return 0;
}
