#include <iostream>

#include <nzmqtpub.h>

using namespace std;

int main(int argc, char *argv[]) {
	// Verifico che siano stati passati i parametri correttamente
	if (argc < 3) {
		cout << "Usage: ./NZMQT_Publisher <[>@]tcp://address:port> <time in ms> <frames ...>" << endl;
		return 0;
	}
	
	// Creo un oggetto PUB e avvio la coda dei messaggi
	NZMQTPub a(argc, argv);
	
	return a.exec();
}
