#ifndef NZMQTPUB_H
#define NZMQTPUB_H

#include <QCoreApplication>
#include <nzmqt/nzmqt.hpp>

class NZMQTPub : public QCoreApplication {
	Q_OBJECT
	
	// Definizione di messaggio
	using Message = QList<QByteArray>;
	
private:
	// Timer
	QTimer *_timer;
	
	// Socket
	nzmqt::ZMQContext *_context;
	nzmqt::ZMQSocket *_socket;
	
	// Message
	Message _message;
	
public:
	// Costruttore
	NZMQTPub(int argc, char **argv);
	
private slots:
	void timeIsOver();
};

#endif // NZMQTPUB_H
