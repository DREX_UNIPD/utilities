#include <iostream>

#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

using namespace std;

void printBits(unsigned char *buffer, const int BUFFER_SIZE) {
	// Stampo il risultato
	for (int i = 0; i < BUFFER_SIZE; i++) {
		for (int b = 7; b >= 0; b--) {
			if (buffer[i] & (1 << b)) {
				cout << "1";
			}
			else {
				cout << "0";
			}
		}
		cout << " ";
	}
	cout << endl;
}

void printHex(unsigned char *buffer, const int BUFFER_SIZE, const char *padding = "") {
	for (int i = 0; i < BUFFER_SIZE; i++) {
		cout << padding;
		cout << hex << ((buffer[i] & 0xf0) >> 4);
		cout << padding;
		cout << hex << (buffer[i] & 0x0f);
		cout << " ";
	}
	cout << endl;
}

int main() {
	int file;
	const char *device = "/dev/i2c-0";
	
	// Vacuometro
	const int BUFFER_SIZE = 4;
	const unsigned char ADDRESS = 0x28;
	
	// Apro il dispositivo
	if ((file = open(device, O_RDWR)) < 0)
		exit(1);
	
	// Imposto l'indirizzo
	if (ioctl(file, I2C_SLAVE, ADDRESS) < 0)
		exit(2);
	
	while (true) {
		// Invio la richiesta di lettura
		unsigned char buffer[BUFFER_SIZE];
		if (read(file, buffer, BUFFER_SIZE) != BUFFER_SIZE)
			exit(3);
		
		// Stampo in bit
		printBits(buffer, BUFFER_SIZE);
		
		// Stampo in hex
		printHex(buffer, BUFFER_SIZE, "   ");
		
		// Converto in pressione il dato ricevuto
		__u16 psirawmin = 0x0666;
		__u16 psirawmax = 0x399A;
		double psimin = 0;
		double psimax = 30;
		__u16 psiraw = buffer[1] + (buffer[0] << 8);
		double psi;
		psi = psimin + (psiraw - psirawmin) * (psimax - psimin) / (psirawmax - psirawmin);
		cout << "Pressure: " << psi << " Psi" << endl;
		
		// Converto in temperatura il dato ricevuto
		__u16 temprawmin = 0x0000;
		__u16 temprawmax = 0x07FF;
		double tempmin = -50;
		double tempmax = 150;
		__u16 tempraw = (buffer[3] >> 5) + (buffer[2] << 3);
		double temp;
		temp = tempmin + (tempraw - temprawmin) * (tempmax - tempmin) / (temprawmax - temprawmin);
		cout << "Temperature: " << temp << " C" << endl;
		
		usleep(10000);
	}
	
	
	// Chiudo il dispositivo
	close(file);
	
	return 0;
}

